﻿using Microsoft.Extensions.Configuration;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.RabbitMQ;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost
{
    public class RabbitMQSettings : IRabbitMQSettings
    {
        public RabbitMQSettings(IConfiguration configuration)
        {
            Host = configuration["RabbitMQ:Host"];
            VirtualHost = configuration["RabbitMQ:VirtualHost"];
            UserName = configuration["RabbitMQ:User"];
            Password = configuration["RabbitMQ:Password"];
        }

        public string UserName { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public string VirtualHost { get; set; }
    }
}
