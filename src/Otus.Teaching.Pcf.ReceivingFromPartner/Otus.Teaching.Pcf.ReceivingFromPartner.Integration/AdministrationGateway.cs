﻿using System;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Dto;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.RabbitMQ;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using RabbitMQ.Client;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class AdministrationGateway
        : IAdministrationGateway
    {
        private readonly IRabbitMQSettings _rabbitMQSettings;

        public AdministrationGateway(IRabbitMQSettings rabbitMQSettings)
        {
            _rabbitMQSettings = rabbitMQSettings;
        }
        
        public async Task NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId)
        {
            var dto = new PartnerManagerCustomerDto()
            {
                PartnerManagerId = partnerManagerId
            };

            Send(dto);
        }

        private IConnection GetRabbitConnection()
        {
            var factory = new ConnectionFactory
            {
                UserName = _rabbitMQSettings.UserName,
                Password = _rabbitMQSettings.Password,
                VirtualHost = _rabbitMQSettings.VirtualHost,
                HostName = _rabbitMQSettings.Host
            };

            var conn = factory.CreateConnection();
            return conn;
        }

        private void Send(PartnerManagerCustomerDto dto)
        {
            using (var connection = GetRabbitConnection())
            using (var channel = connection.CreateModel())
            {
                var body = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(dto));
                channel.BasicPublish(string.Empty, "PartnerManagerPromoQueue", null, body);
            }
        }
    }
}