﻿using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Dto;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using RabbitMQ.Client;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.RabbitMQ;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class GivingPromoCodeToCustomerGateway
        : IGivingPromoCodeToCustomerGateway
    {
        private readonly IRabbitMQSettings _rabbitMQSettings;

        public GivingPromoCodeToCustomerGateway(IRabbitMQSettings rabbitMQSettings)
        {
            _rabbitMQSettings = rabbitMQSettings;
        }
        
        public async Task GivePromoCodeToCustomer(PromoCode promoCode)
        {
            var dto = new GivePromoCodeToCustomerDto()
            {
                PartnerId = promoCode.Partner.Id,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PreferenceId = promoCode.PreferenceId,
                PromoCode = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                PartnerManagerId = promoCode.PartnerManagerId
            };

            Send(dto);
        }

        private IConnection GetRabbitConnection()
        {
            var factory = new ConnectionFactory
            {
                UserName = _rabbitMQSettings.UserName,
                Password = _rabbitMQSettings.Password,
                VirtualHost = _rabbitMQSettings.VirtualHost,
                HostName = _rabbitMQSettings.Host
            };

            var conn = factory.CreateConnection();
            return conn;
        }

        public void Send(GivePromoCodeToCustomerDto dto)
        {
            using (var connection = GetRabbitConnection())
            using (var channel = connection.CreateModel())
            {
                var body = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(dto));
                channel.BasicPublish(string.Empty, "GivePromoCodeToCustomerQueue", null, body);
            }
        }
    }
}