﻿namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.RabbitMQ
{
    public interface IRabbitMQSettings
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public string VirtualHost { get; set; }
    }
}
