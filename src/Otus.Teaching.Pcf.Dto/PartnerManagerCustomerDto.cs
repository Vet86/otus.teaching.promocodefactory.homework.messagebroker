﻿using System;

namespace Otus.Teaching.Pcf.Dto
{
    public class PartnerManagerCustomerDto
    {
        public Guid PartnerManagerId { get; set; }
    }
}
