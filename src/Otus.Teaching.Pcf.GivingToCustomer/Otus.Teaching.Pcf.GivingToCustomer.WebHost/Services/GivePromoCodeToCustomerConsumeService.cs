﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.Pcf.Core;
using Otus.Teaching.Pcf.Dto;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Processing;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services
{
    public class GivePromoCodeToCustomerConsumeService : BackgroundService
    {
        private readonly IPromoCodesProcessing _promoCodesProcessing;
        private readonly Consumer _consumer;

        public GivePromoCodeToCustomerConsumeService(
            IConfiguration configuration,
            IPromoCodesProcessing promoCodesProcessing
            )
        {
            _promoCodesProcessing = promoCodesProcessing;
            _consumer = new Consumer(
                    configuration["RabbitMQ:Host"],
                    configuration["RabbitMQ:VirtualHost"],
                    configuration["RabbitMQ:User"],
                    configuration["RabbitMQ:Password"],
                    "GivePromoCodeToCustomerQueue");
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            if (stoppingToken.IsCancellationRequested)
            {
                _consumer.Dispose();
                return Task.CompletedTask;
            }
            _consumer.ReceiveMessage(HandleMessage);

            return Task.CompletedTask;
        }

        private async void HandleMessage(string message)
        {
            var givePromoCodeToCustomer = JsonSerializer.Deserialize<GivePromoCodeToCustomerDto>(message);

            try
            {
                await _promoCodesProcessing.AddPromoCode(new Pcf.Models.GivePromoCodeRequest
                {
                    BeginDate = givePromoCodeToCustomer.BeginDate,
                    EndDate = givePromoCodeToCustomer.EndDate,
                    PartnerId = givePromoCodeToCustomer.PartnerId,
                    PreferenceId = givePromoCodeToCustomer.PreferenceId,
                    PromoCodeId = givePromoCodeToCustomer.PromoCodeId,
                    PromoCode = givePromoCodeToCustomer.PromoCode,
                    ServiceInfo = givePromoCodeToCustomer.ServiceInfo
                });
            }
            catch (KeyNotFoundException)
            {
                return;
            }
        }
    }
}
