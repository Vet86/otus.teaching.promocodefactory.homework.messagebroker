﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.Models;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Processing
{
    public interface IPromoCodesProcessing
    {
        Task AddPromoCode(GivePromoCodeRequest givePromoCodeRequest);
        IRepository<PromoCode> PromoCodesRepository { get; }
    }
}
