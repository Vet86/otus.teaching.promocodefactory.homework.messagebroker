﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Processing;
using Otus.Teaching.Pcf.Core;
using Otus.Teaching.Pcf.Dto;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services
{
    public class PartnerManagerPromoConsumeService : BackgroundService
    {
        private readonly Consumer _consumer;
        private readonly IEmployeeProcessing _employeeProcessing;

        public PartnerManagerPromoConsumeService(
            IEmployeeProcessing employeeProcessing,
            IConfiguration configuration
            )
        {
            _employeeProcessing = employeeProcessing;
            _consumer = new Consumer(
                    configuration["RabbitMQ:Host"],
                    configuration["RabbitMQ:VirtualHost"],
                    configuration["RabbitMQ:User"],
                    configuration["RabbitMQ:Password"],
                    "PartnerManagerPromoQueue");
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            if (stoppingToken.IsCancellationRequested)
            {
                _consumer.Dispose();
                return Task.CompletedTask;
            }
            _consumer.ReceiveMessage(HandleMessage);

            return Task.CompletedTask;
        }

        private async void HandleMessage(string message)
        {
            var partnerManagerCustomerDto = JsonSerializer.Deserialize<PartnerManagerCustomerDto>(message);
            try
            {
                await _employeeProcessing.UpdateEmployee(partnerManagerCustomerDto.PartnerManagerId);
            }
            catch (KeyNotFoundException)
            {
                return;
            }
        }
    }
}
