﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Processing;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Processing
{
    public class EmployeeProcessing : IEmployeeProcessing
    {
        private readonly IRepository<Employee> _employeeRepository;

        public IRepository<Employee> EmployeeRepository => _employeeRepository;

        public EmployeeProcessing(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public async Task UpdateEmployee(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                throw new KeyNotFoundException();

            employee.AppliedPromocodesCount++;

            await _employeeRepository.UpdateAsync(employee);
        }
    }
}
