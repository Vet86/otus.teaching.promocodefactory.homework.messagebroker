﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Abstractions.Processing
{
    public interface IEmployeeProcessing
    {
        Task UpdateEmployee(Guid id);

        IRepository<Employee> EmployeeRepository { get; }
    }
}
